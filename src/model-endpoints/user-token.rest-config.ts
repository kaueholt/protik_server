import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {UserToken} from '../models';

const config: ModelCrudRestApiConfig = {
  model: UserToken,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/user-tokens',
};
module.exports = config;
