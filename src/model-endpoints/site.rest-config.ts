import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Site} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Site,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/sites',
};
module.exports = config;
