import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketTask} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketTask,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-tasks',
};
module.exports = config;
