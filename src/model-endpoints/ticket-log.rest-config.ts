import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketLog} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketLog,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-logs',
};
module.exports = config;
