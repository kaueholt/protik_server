import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {StatusType} from '../models';

const config: ModelCrudRestApiConfig = {
  model: StatusType,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/status-types',
};
module.exports = config;
