import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Status} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Status,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/statuses',
};
module.exports = config;
