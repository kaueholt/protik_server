import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketTag} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketTag,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-tags',
};
module.exports = config;
