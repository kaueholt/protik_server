import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketType} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketType,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-types',
};
module.exports = config;
