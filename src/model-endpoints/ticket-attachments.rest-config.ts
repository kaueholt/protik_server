import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketAttachments} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketAttachments,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-attachments',
};
module.exports = config;
