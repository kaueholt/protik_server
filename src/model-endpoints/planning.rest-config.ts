import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Planning} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Planning,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/plannings',
};
module.exports = config;
