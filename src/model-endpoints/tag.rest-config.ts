import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Tag} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Tag,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/tags',
};
module.exports = config;
