import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketChecklist} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketChecklist,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-checklists',
};
module.exports = config;
