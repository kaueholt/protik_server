import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {TicketComment} from '../models';

const config: ModelCrudRestApiConfig = {
  model: TicketComment,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/ticket-comments',
};
module.exports = config;
