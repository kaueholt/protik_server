import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Person} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Person,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/people',
};
module.exports = config;
