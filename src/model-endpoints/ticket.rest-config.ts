import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Ticket} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Ticket,
  pattern: 'CrudRest',
  dataSource: 'protikmysql',
  basePath: '/tickets',
};
module.exports = config;
