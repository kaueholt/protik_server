import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'protikmysql',
  connector: 'mysql',
  url: '',
  host: 'kaue',
  port: 3306,
  user: 'robot',
  password: 'adm123',
  database: 'protik'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class ProtikmysqlDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'protikmysql';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.protikmysql', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
