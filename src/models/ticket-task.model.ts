import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: true, mysql: {schema: 'protik', table: 'ticket_task'}}
})
export class TicketTask extends Entity {
  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'date',
    mysql: {columnName: 'date', dataType: 'date', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  date?: string;

  @property({
    type: 'string',
    mysql: {columnName: 'hour_start', dataType: 'time', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  hourStart?: string;

  @property({
    type: 'string',
    mysql: {columnName: 'hour_end', dataType: 'time', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  hourEnd?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'description', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  description?: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'user_person_id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  userPersonId: number;

  @property({
    type: 'number',
    precision: 10,
    scale: 0,
    mysql: {columnName: 'create_user', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'Y'},
  })
  createUser?: number;

  @property({
    type: 'number',
    precision: 10,
    scale: 0,
    mysql: {columnName: 'update_user', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'Y'},
  })
  updateUser?: number;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'ticket_id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  ticketId: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TicketTask>) {
    super(data);
  }
}

export interface TicketTaskRelations {
  // describe navigational properties here
}

export type TicketTaskWithRelations = TicketTask & TicketTaskRelations;
